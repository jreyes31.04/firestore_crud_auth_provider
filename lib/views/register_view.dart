import 'package:crud_firebase/views/chargin_view.dart';
import 'package:crud_firebase/widgets/custom_text_button.dart';
import 'package:flutter/material.dart';

import '../firebase/auth.dart';
import '../utils/validator.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({
    Key? key,
  }) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _nameController = TextEditingController();

  final TextEditingController _ageController = TextEditingController();

  final TextEditingController _usernameController = TextEditingController();

  bool _isSendingData = false;

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Center(
          child: Visibility(
        //Este widget ya lo conocen, de Feed
        visible: !_isSendingData, //Ese ! al inicio es para hacer una negación
        replacement: //o sea que si está enviando datos muestre este
            const ChargingView(text: 'Registrando user, por favor espere'),
        //Y si no, muestre este
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                Text(
                  'Formulario de registro super elaborado :)',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: screenHeight * 0.03),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    labelText: 'Nombre',
                    constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                  ),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                TextFormField(
                  controller: _ageController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Edad',
                    constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                  ),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    labelText: 'Pseudónimo',
                    constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                  ),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    labelText: 'Email',
                    constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                  ),
                  validator: (String? value) =>
                      Validator.validateField(value: value!),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                  ),
                  validator: (String? value) =>
                      Validator.validateField(value: value!),
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                CustomTextButton(
                  callback: () {
                    setState(() {
                      _isSendingData =
                          true; //Esto es solo para que ponga la pantalla de carga
                    });
                    CustomAuth.registerWithEmailAndPassword(
                      //Llamamos la función de registro
                      email: _emailController.text,
                      pass: _passwordController.text,
                      userName: _usernameController.text,
                      age: _ageController.text,
                      name: _nameController.text,
                      context: context,
                    ).then((value) => Navigator.pop(
                        context)); //Y luego le decimos que quite esta vista (Una vez que haya registrado el user y lo haya mandado a la DB)
                  },
                  text: 'Registrarse',
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
