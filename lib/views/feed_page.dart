import 'package:crud_firebase/firebase/auth.dart';
import 'package:crud_firebase/provider/user_provider.dart';
import 'package:crud_firebase/views/chargin_view.dart';
import 'package:crud_firebase/widgets/custom_text_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FeedPage extends StatelessWidget {
  const FeedPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Visibility(
        //Este widget nos permite cargar un widget u otro dependiendo de un bool
        visible: context.watch<UserProvider>().customUser.uid.isNotEmpty,
        //Este es el bool usado, es decir mientras el provider tenga el uid como '' nos mandará el replacement
        replacement: const ChargingView(
          text: 'Cargando datos desde la DB',
        ), //El replacement es solo la vista de carga
        child: Column(
          mainAxisAlignment: MainAxisAlignment
              .spaceEvenly, //Esto es para ordenar los elementos
          children: [
            const Text(
              "Esta feed page es solo un ejemplo, desde acá se puede disparar a otras vistas y flujos. De igual forma, desde una vista de registro se pueden obtener más datos del usuario y meterlos en el provider y en Firestore DB según corresponda.",
              textAlign: TextAlign.center,
            ),
            //En adelante consultamos los datos desde el providers
            Text('Email: ${context.watch<UserProvider>().customUser.email}'),
            Text('Uid: ${context.watch<UserProvider>().customUser.uid}'),
            Text('Nombre: ${context.watch<UserProvider>().customUser.name}'),
            Text('Edad: ${context.watch<UserProvider>().customUser.age}'),
            Text(
                'UserName: ${context.watch<UserProvider>().customUser.userName}'),
            CustomTextButton(
              callback: () {
                CustomAuth.signOut(
                    context:
                        context); //Un botón para cerrar sesión con la función implementada en CustomAuth
              },
              text: 'Cerrar sesión',
            ),
          ],
        ),
      )),
    );
  }
}
