import 'package:crud_firebase/firebase/auth.dart';
import 'package:crud_firebase/utils/validator.dart';
import 'package:crud_firebase/views/register_view.dart';
import 'package:crud_firebase/widgets/custom_text_button.dart';
import 'package:flutter/material.dart';

class SignIn extends StatelessWidget {
  SignIn({super.key});
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Text(
                "Inicia sesión con correo y contraseña",
              ),
              SizedBox(height: screenHeight * 0.05),
              TextFormField(
                controller: _emailController,
                decoration: InputDecoration(
                  labelText: 'Email',
                  constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                ),
                validator: (String? value) =>
                    Validator.validateField(value: value!),
              ),
              SizedBox(height: screenHeight * 0.05),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  labelText: 'Password',
                  constraints: BoxConstraints(maxWidth: screenWidth * 0.8),
                ),
                validator: (String? value) =>
                    Validator.validateField(value: value!),
              ),
              SizedBox(height: screenHeight * 0.05),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CustomTextButton(
                    text: 'Iniciar sesión',
                    callback: () {
                      CustomAuth.signInWithEmailAndPassword(
                        email: _emailController.text,
                        pass: _passwordController.text,
                        context: context,
                      );
                    },
                  ),
                  CustomTextButton(
                    text: 'Registrarse',
                    callback: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: ((context) => const RegisterView()),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
